import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { Theme } from "@material-ui/core/styles";
import { createStyles } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/core/styles";
import React from "react";

export function Home() {
  const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      root: {
        flexGrow: 1,
      },
      paper: {
        padding: theme.spacing(0),
        textAlign: "center",
        color: "theme.palette.text.secondary",
      },
      magic: {
        width: "50vw",
        height: "100vh",
        margin: " 10vh auto",
        justifyContent: "center", //Centered vertically
        alignItems: "center", // Centered horizontally
        flexDirection: "column",
        flex: 1,
        display: "flex",
        background: "rgba(0,0,0,0)",
      },
    })
  );

  const classes = useStyles();

  return (
    <div>
      <div className={classes.root}>
        <Grid container justify="center" spacing={0}>
          <Grid item xs={12}>
            <div
              style={{
                height: "35vh",
              }}
            ></div>
          </Grid>
          <Grid item xs={12} sm={6}>
            <Paper
              className={classes.paper}
              style={{
                backgroundColor: "transparent",
              }}
              elevation={0}
            >
              <div
                className={classes.magic}
                style={{
                  height: "20vh",
                  margin: "0 auto",
                }}
              >
                <img
                  src="illustrations/Home.png"
                  alt="Home Image"
                  style={{
                    width: "40vw",
                  }}
                />
              </div>
            </Paper>
          </Grid>
          <Grid item xs={12} sm={6}>
            <Paper
              className={classes.paper}
              style={{
                height: "20vh",
                backgroundColor: "transparent",
                justifyContent: "center", //Centered vertically
              }}
              elevation={0}
            >
              <div
                className={classes.magic}
                style={{
                  height: "10vh",
                  paddingTop: "2vh",
                }}
              >
                <h1 style={{}}>Cryptoboy</h1>
                <h4
                  style={{
                    textAlign: "center",
                  }}
                >
                  Solusi keren belajar kriptografi
                </h4>
              </div>
            </Paper>
          </Grid>
          <Grid item xs={12}>
            <div
              style={{
                height: "10vh",
                paddingTop: "23vh",
                backgroundColor: "transparent",
              }}
            >
              <h6
                style={{
                  textAlign: "center",
                }}
              >
                Mulai jelajahi dengan menu di sebelah kiri! Selamat mencoba.
              </h6>
            </div>
          </Grid>
        </Grid>
      </div>
    </div>
  );
}
