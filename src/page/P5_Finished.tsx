import { createStyles } from "@material-ui/core/styles";
import makeStyles from "@material-ui/core/styles/makeStyles";
import { Button } from "@material-ui/core";
import Grid from "@material-ui/core/Grid/Grid";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import React from "react";
import { deskripsi, judul, kelar, lihat } from "../constants/Text";
import { Theme } from "@material-ui/core/styles/createMuiTheme";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      padding: theme.spacing(0),
      textAlign: "center",
    },
  })
);

interface FinishedProps {
  Cipher: number;
  setCipher: (popol: number) => void;
  Stage: number;
  setStage: (popol: number) => void;
}

export function Finished(Props: FinishedProps) {
  const { Cipher, setCipher, Stage, setStage } = Props;
  const classes = useStyles();
  return (
    <div
      style={{
        marginLeft: "2rem",
        marginRight: "2rem",
        flexGrow: 1,
      }}
    >
      <Grid container spacing={0}>
        <Grid
          item
          style={{
            height: "60vh",
            textAlign: "center",

            paddingTop: "20vh",
          }}
          xs={12}
        >
          <img
            src="illustrations/Home.png"
            alt="skidi"
            style={{
              height: "40vh",
            }}
          />
        </Grid>
        <Grid
          item
          style={{
            height: "20vh",
            textAlign: "center",
            marginTop: "2vh",
          }}
          xs={12}
        >
          <h1>Hore</h1>
          <h3>{Cipher % 2 === 0 ? kelar[0] : kelar[1]}</h3>
          <h4>{Cipher % 2 === 0 ? lihat[0] : lihat[1]}</h4>
        </Grid>
        <Grid
          item
          style={{
            height: "10vh",
            textAlign: "center",
            marginTop: "7vh",
          }}
          xs={12}
        >
          <h2>Hasil Pesan</h2>
        </Grid>
        {Cipher === 5 ? (
          <Grid container xs={12} spacing={0}>
            <Grid item xs={6}>
              <Paper
                className={classes.paper}
                style={{
                  height: "35vh",
                  backgroundColor: "transparent",
                  textAlign: "center",
                }}
                elevation={0}
              >
                <TextField
                  rows={12}
                  label="Masukkan key"
                  placeholder="Teks"
                  multiline
                  variant="outlined"
                  style={{
                    width: "95%",
                  }}
                />
              </Paper>
            </Grid>
            <Grid item xs={6}>
              <Paper
                className={classes.paper}
                style={{
                  height: "15vh",
                  backgroundColor: "transparent",
                  textAlign: "center",
                }}
                elevation={0}
              >
                <TextField
                  rows={12}
                  label="Masukkan Plaintext"
                  placeholder="Teks"
                  multiline
                  variant="outlined"
                  style={{
                    width: "95%",
                  }}
                />
              </Paper>
            </Grid>
          </Grid>
        ) : (
          // Bukan enkripsiin dokumen
          <Grid item xs={12}>
            <Paper
              className={classes.paper}
              style={{
                height: "45vh",
                backgroundColor: "transparent",
                textAlign: "left",
              }}
              elevation={0}
            >
              <h3>Isi pesansaidoajsodiasjdioajoashjdoiasodjiasodn</h3>
            </Paper>
          </Grid>
        )}
        <Grid item xs={12}>
          <Paper
            className={classes.paper}
            style={{
              height: "10vh",
              marginTop: "3rem",
              textAlign: "center",
              backgroundColor: "transparent",
              flexDirection: "row",
              display: "flex",
            }}
            elevation={0}
          >
            <Button
              style={{
                marginLeft: "1.5rem",
                marginTop: "0.5rem",
                height: "2.5rem",
                backgroundColor: "#FEB3B0",
              }}
              variant="contained"
              size="medium"
              component="span"
              onClick={() => {
                setStage(0);
              }}
            >
              Kembali ke Home
            </Button>
            {/* Simpan input */}
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
}
