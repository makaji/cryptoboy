import { Grid } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import { Theme } from "@material-ui/core/styles";
import { createStyles } from "@material-ui/core/styles";
import makeStyles from "@material-ui/core/styles/makeStyles";
import TextField from "@material-ui/core/TextField";
import React from "react";
import { deskripsi, judul, pilihan } from "../constants/Text";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      padding: theme.spacing(0),
      textAlign: "center",
    },
  })
);

interface PromptInputProps {
  Cipher: number;
  Stage: number;
  setStage: (popol: number) => void;
}

export function PromptInput(Props: PromptInputProps) {
  const classes = useStyles();
  const { Cipher, Stage, setStage } = Props;
  return (
    <div
      style={{
        marginLeft: "2rem",
        marginRight: "2rem",
        flexGrow: 1,
      }}
    >
      <Grid container spacing={0}>
        <Grid
          item
          style={{
            height: "25vh",
          }}
          xs={12}
        >
          <h1>{judul[(Cipher - (Cipher % 2)) / 2]}</h1>
          <h4>{deskripsi[(Cipher - (Cipher % 2)) / 2]}</h4>
        </Grid>
        <Grid
          item
          style={{
            height: "12vh",
            textAlign: "center",
          }}
          xs={12}
        >
          <h2>{Cipher % 2 === 0 ? pilihan[0] : pilihan[1]}</h2>
        </Grid>
        <Grid item xs={12}>
          <Paper
            className={classes.paper}
            style={{
              height: "35vh",
              backgroundColor: "transparent",
              textAlign: "center",
            }}
            elevation={0}
          >
            <TextField
              rows={12}
              label={Cipher % 2 === 0 ? pilihan[0] : pilihan[1]}
              placeholder="Teks"
              multiline
              variant="outlined"
              style={{
                width: "95%",
              }}
            />
          </Paper>
        </Grid>
        <Grid item xs={12}>
          <Paper
            className={classes.paper}
            style={{
              height: "10vh",
              marginTop: "3rem",
              textAlign: "center",
              backgroundColor: "transparent",
              flexDirection: "row",
              display: "flex",
            }}
            elevation={0}
          >
            {/* <img
              src="illustrations/decrypt.png"
              alt="skidi"
              style={{
                height: "30vh",
              }}
            /> */}
            <Button
              style={{
                marginLeft: "1.5rem",
                marginTop: "0.5rem",
                height: "2.5rem",
                backgroundColor: "#FEB3B0",
              }}
              variant="contained"
              size="medium"
              component="span"
              onClick={() => {
                setStage(Stage + 1);
              }}
            >
              LANJUT
            </Button>
            {/* Simpan input */}
            <h4
              style={{
                marginLeft: "1rem",
                marginRight: "1rem",
              }}
            >
              atau masukkan file
            </h4>
            <input
              //   accept=".jpg, .pdf, .docx,.txt"
              style={{
                display: "none",
              }}
              id="contained-button-file"
              multiple
              type="file"
            />
            <label htmlFor="contained-button-file">
              <Button
                style={{
                  marginTop: "0.5rem",
                  height: "2.5rem",
                  backgroundColor: "#FEB3B0",
                  color: "black",
                }}
                variant="contained"
                color="primary"
                component="span"
              >
                UPLOAD
              </Button>
            </label>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
}
