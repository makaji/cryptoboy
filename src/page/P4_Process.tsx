import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid/Grid";
import Paper from "@material-ui/core/Paper";
import { Theme } from "@material-ui/core/styles/createMuiTheme";
import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import React from "react";
import { proses } from "../constants/Text";
import Loader from "react-loader-spinner";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      padding: theme.spacing(0),
      textAlign: "center",
    },
    root: {
      flexGrow: 1,
    },
  })
);

interface ProcessProps {
  Cipher: number;
  setCipher: (popol: number) => void;
  Stage: number;
  setStage: (popol: number) => void;
}

export function Process(Props: ProcessProps) {
  const classes = useStyles();
  const { Cipher, setCipher, Stage, setStage } = Props;
  return (
    <div>
      <div className={classes.root}>
        <Grid container justify="center" spacing={0}>
          <Grid item xs={12}>
            <div
              style={{
                height: "10vh",
              }}
            ></div>
          </Grid>
          <Grid item xs={12}>
            <Paper
              className={classes.paper}
              style={{
                height: "10vh",
                backgroundColor: "transparent",
                textAlign: "center",
                marginTop: "20vh",
              }}
              elevation={0}
            >
              <Loader type="Puff" color="#00BFFF" height={160} width={160} />
            </Paper>
          </Grid>
          <Grid item xs={12}>
            <Paper
              className={classes.paper}
              style={{
                height: "30vh",
                marginTop: "15vh",
                backgroundColor: "transparent",
              }}
              elevation={0}
            >
              <h1
                style={{
                  padding: "0 2vh",
                  textAlign: "center",
                }}
              >
                Processing...
              </h1>
              <h4
                style={{
                  padding: "0 2vh",
                  textAlign: "center",
                }}
              >
                {Cipher % 2 === 0 ? proses[0] : proses[1]}
              </h4>
            </Paper>
          </Grid>
          <Grid
            style={{
              textAlign: "center",
            }}
            item
            xs={12}
          >
            <Button
              style={{
                marginLeft: "1.5rem",
                marginTop: "0.5rem",
                height: "2.5rem",
                backgroundColor: "#FEB3B0",
                textAlign: "center",
              }}
              variant="contained"
              size="medium"
              component="span"
              onClick={() => {
                setStage(Stage + 1);
              }}
            >
              Ini Proses
            </Button>
          </Grid>
        </Grid>
      </div>
    </div>
  );
}
